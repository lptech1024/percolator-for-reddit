Reddit content filtering

# What's this about?

John likes to browse [/r/MealPrepSunday](https://www.reddit.com/r/MealPrepSunday/).\
He is allergic to eggs.\
Posts featuring eggs don't deserve a downvote, but he would prefer to view content relevant to his dietary needs.\
He decides to automatically filter out posts with eggs in the title.
