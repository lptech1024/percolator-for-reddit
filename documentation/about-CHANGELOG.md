# Guidelines

* Describe the user effect (what) and context (where and why)
* Use reverse chronology for release ordering
* Link to merge requests
* Include a changelog entry for a
    - user-facing change
    - security fix
    - change that introduces a database migration
* Do not include a changelog entry for a
    - developer-facing change
    - regression introduced and fixed in the same release

# Structure

```
[{MAJOR}.{MINOR}.{PATCH}] YANKED
{YYYY}-{MM}-{DD}
```

# Samples

[3.0.0]\
2031-12-22

* Security: User API leaked email address

[2.0.0] YANKED\
2031-12-21

* Added: User API to request usernames

[1.0.2]\
2030-01-02

* Performance: faster user creation with parallel requests

# Description headers

* Added
* Changed
* Deprecated
* Fixed
* Other
* Performance
* Removed
* Security

# References

[Keep a Changelog](https://keepachangelog.com/en/1.0.0/)\
[freeCodeCamp](https://www.freecodecamp.org/news/a-beginners-guide-to-git-what-is-a-changelog-and-how-to-generate-it/)\
[Amoeboids](https://amoeboids.com/blog/changelog-how-to-write-good-one/)\
[GitLab development](https://docs.gitlab.com/ee/development/changelog.html)
