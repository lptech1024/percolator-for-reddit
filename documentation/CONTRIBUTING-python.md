# Standard

Default to [PEP 8](https://peps.python.org/pep-0008/)

# Default overrides

## Code Lay-out

### Indentation

Use one tab per indentation level

Use hanging indents for multiline constructs\
The closing brace/bracket/parenthesis on multiline constructs should be lined up under the first character of the line that starts the multiline construct, as in:
```
def function_name(
	argument_one,
	argument_two,
	argument_three
):

my_list = [
	'item_one',
	'item_two',
	'item_three'
]

result = function_with_arguments(
	'argument_one',
	'argument_two',
	3
)
```

### Maximum Line Length

Limit lines to a maximum of 100 characters.
