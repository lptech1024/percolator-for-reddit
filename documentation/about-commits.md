# Structure

```
{Type}{(optional scope)}: {description}

{body}

{optional footer}
```


# Contents

Use imperative mood - "Add" not "added" or "adds"

## Summary

* <= 50 characters
* Capitalize - "Add: cool feature"
* Do not end with period (okay if last word is "St.")
* Do not include ticket IDs

### Types

* Add: Create a capability e.g. feature, test, dependency
* Bump: Increase the version of something e.g. a dependency
* Doc: A change that MUST be only in the documentation, e.g. help files
* Drop: Delete a capability e.g. feature, test, dependency
* Fix: Fix an issue e.g. bug, typo, accident, misstatement
* Make: Change the build process, tools, or infrastructure
* Perf: A change that MUST be just about performance, e.g. speed up code
* Refactor: A change that MUST be just refactoring. e.g. rename variable
* Start: Begin doing something; e.g. enable a toggle, feature flag
* Stop: End doing something; e.g. disable a toggle, feature flag
* Style: A change that MUST be just formatting, e.g. change spaces

## Body

* Wrap at 72 characters
* Explain what and why rather than how

# Signing

[GitLab](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/)\
[GitHub](https://docs.github.com/en/authentication/managing-commit-signature-verification)

## Hardware Keys

### YubiKey
[YubiKey and OpenPGP](https://support.yubico.com/hc/en-us/articles/360013790259-Using-Your-YubiKey-with-OpenPGP)\
[YubiKey and GPG Troubleshooting](https://support.yubico.com/hc/en-us/articles/360013714479-Troubleshooting-Issues-with-GPG)\
[Missing device in Linux](https://bugzilla.redhat.com/show_bug.cgi?id=1893131)

# References

[Chris Beams](https://cbea.ms/git-commit/)\
[Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)\
[Joel Henderson](https://github.com/joelparkerhenderson/git-commit-message)
