[requests](https://pypi.org/project/requests/)

# Dates

Last Reviewed: Jan 2023

Project started: Feb 2011 (12 years)

[Last Updated](https://github.com/psf/requests/commits/main): Jan 2023

[Last Released](https://github.com/psf/requests/releases): Jan 2023

[Contributers](https://github.com/psf/requests/graphs/contributors) in the last year: 2

# Details

Source: https://github.com/psf/requests

License: [Apache 2.0](https://github.com/psf/requests/blob/main/LICENSE)

CI: [Yes](https://github.com/psf/requests/actions)

Documentation: [Yes](https://requests.readthedocs.io/en/latest/)

Issues: [Yes](https://github.com/psf/requests/issues)

PRs: [Yes](https://github.com/psf/requests/pulls)

Security: [Vulnerability Disclosure](https://github.com/psf/requests/security/policy)

Data: developer-controlled

# Purpose

HTTP requests such as to the Reddit API
