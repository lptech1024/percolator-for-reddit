[flask-marshmallow](https://github.com/marshmallow-code/flask-marshmallow)

# Dates

Last Reviewed: May 2022

Project started: Apr 2014 (8 years)

[Last Updated](https://github.com/marshmallow-code/flask-marshmallow/commits/dev): Jan 2022

[Last Released](https://github.com/marshmallow-code/flask-marshmallow/tags): Sep 2020

[Contributers](https://github.com/marshmallow-code/flask-marshmallow/graphs/contributors) in the last year: 1

# Details

License: [MIT](https://github.com/marshmallow-code/flask-marshmallow/blob/dev/LICENSE)

CI: [Yes](https://dev.azure.com/sloria/sloria/_build?definitionId=14&_a=summary)

Documentation: [Yes](https://flask-marshmallow.readthedocs.io/en/latest/)

Issues: [Yes](https://github.com/marshmallow-code/flask-marshmallow/issues)

PRs: [Yes](https://github.com/marshmallow-code/flask-marshmallow/pulls)

Security: no issue tags or relevant documentation

Data: local only

# Purpose

Integrates [Flask](https://flask.palletsprojects.com/en/2.1.x/) and [marshmallow](https://marshmallow.readthedocs.io/en/stable/)

Adds URL and hyperlink fields ([HATEOAS](https://en.wikipedia.org/wiki/HATEOAS))

# Notes

Optional [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/) integration

Project isn't very active
