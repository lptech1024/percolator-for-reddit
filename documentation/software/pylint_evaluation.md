[pylint](https://pypi.org/project/pylint/)

# Dates

Last Reviewed: Jan 2023

Project started: [2003](https://pylint.readthedocs.io/en/latest/whatsnew/0/0.x.html#what-s-new-in-pylint-0-1)

[Last Updated](https://github.com/PyCQA/pylint/commits/main): Jan 2023

[Last Released](https://github.com/PyCQA/pylint/releases): Jan 2023

[Contributers](https://github.com/PyCQA/pylint/graphs/contributors) in the last year: 45

# Details

Source: https://github.com/PyCQA/pylint

License: [GPL v2.0](https://github.com/PyCQA/pylint/blob/main/LICENSE)

CI: [Yes](https://github.com/PyCQA/pylint/actions)

Documentation: [Yes](https://pylint.pycqa.org/en/latest/)

Issues: [Yes](https://github.com/PyCQA/pylint/issues)

PRs: [Yes](https://github.com/PyCQA/pylint/pulls)

Security: [Third-party](https://tidelift.com/docs/security)

Data: local

# Purpose

Static code analysis for Python

# Notes

Thorough, partial PEP 8\
Slow compared to other linters

As of the initial evaluation, we have the maximum score with ~1 override per 400 lines
