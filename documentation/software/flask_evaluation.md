[Flask](https://pypi.org/project/Flask/)

# Dates

Last Reviewed: Mar 2023

Project started: [2010](https://github.com/pallets/flask/tags?after=0.7)

[Last Updated](https://github.com/pallets/flask/commits/main): Mar 2023

[Last Released](https://github.com/pallets/flask/releases): Feb 2023

[Contributers](https://github.com/pallets/flask/graphs/contributors) in the last year: 8

# Details

Source: https://github.com/pallets/flask

License: [BSD 3-Clause](https://github.com/pallets/flask/blob/main/LICENSE.rst)

CI: [Yes](https://github.com/pallets/flask/actions)

Documentation: [Yes](https://flask.palletsprojects.com/en/latest/quickstart/)

Issues: [Yes](https://github.com/pallets/flask/issues)

PRs: [Yes](https://github.com/pallets/flask/pulls)

Security: [Security policy](https://github.com/pallets/flask/security/policy)

Data: local

# Purpose

Web application microframework

# Notes

Django alternative
