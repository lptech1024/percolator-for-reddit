import time

from sqlalchemy import select

from models.oauth import Token
from models.task import GetUserIdentity, FilterListing, HideLink
from models.common import db
from models.listing import get_matched_listings, get_fullname
from reddit_api import RateLimit, get_user_identity, get_listing, hide_from_listing

check_user_identity = True
check_hide_link = True
check_filter_listing = True

schedule_running = False

last_call_time = time.monotonic()
last_rate_limit = RateLimit(1, 1)

def check_queues():
	global schedule_running
	global check_user_identity
	global check_hide_link
	global check_filter_listing
	global last_call_time
	global last_rate_limit
	schedule_running = True

	while (time.monotonic() - last_call_time) < 1:
		pass

	if check_user_identity:
		get_user_identity = db.session.scalar(select(GetUserIdentity).order_by(GetUserIdentity.created_date).limit(1))
		if get_user_identity is None:
			check_user_identity = False
		else:
			user_identity, last_rate_limit, last_call_time, potential_token = get_user_identity(token=get_user_identity.token)
			db.session.add(user_identity)

			if potential_token is not None:
				get_user_identity.token.update(potential_token)
			db.session.delete(get_user_identity)
			db.session.commit()
	elif check_hide_link:
		hide_link = db.session.scalar(select(HideLink).order_by(HideLink.created_date).limit(1))
		if hide_link is None:
			check_hide_link = False
		else:
			last_rate_limit, last_call_time, potential_token = hide_from_listing(token=hide_link.token, fullnames=hide_link.link_id)

			if potential_token is not None:
				hide_link.token.update(potential_token)
			db.session.delete(hide_link)
			db.session.commit()
	elif check_filter_listing:
		filter_listing = db.session.scalar(select(FilterListing).order_by(FilterListing.created_date).limit(1))
		if filter_listing is None:
			check_filter_listing = False
		else:
			listing_json, last_rate_limit, last_call_time, potential_token = get_listing(
				token=filter_listing.token,
				listing_path=filter_listing.url,
				before=filter_listing.before
			)

			if listing_json and listing_json.data and listing_json.data.children:
				rules = filter_listing.token.account.rules
				matched_fullnames = get_matched_listings(listing_json=listing_json, rules=rules)
				queue_hide_link(token=filter_listing.token, link_fullnames=matched_fullnames)
				remaining_count = filter_listing.remaining_count - len(listing_json.data.children)
				if remaining_count > 0:
					queue_filter_listing(
						token=filter_listing.token,
						url=filter_listing.url,
						result_count=remaining_count,
						before=get_fullname(listing_json.data.children[-1])
					)

			if potential_token is not None:
				filter_listing.token.update(potential_token)
			db.session.delete(filter_listing)
			db.session.commit()

	if check_user_identity or check_hide_link or check_filter_listing:
		check_queues()
	else:
		schedule_running = False

def activate_queue():
	if not schedule_running:
		check_queues()

def queue_get_user_identity(token: Token):
	global check_user_identity

	check_user_identity = True
	get_user_identity = GetUserIdentity(oauth_token_id=token.id)
	db.session.add(get_user_identity)
	db.session.commit()
	activate_queue()

def queue_filter_listing(token: Token, url: str, result_count: int, before: str | None = None):
	global check_filter_listing

	check_filter_listing = True
	filter_listing = FilterListing(
		oauth_token_id=token.id,
		url=url,
		remaining_count=result_count,
		before=before
	)
	db.session.add(filter_listing)
	db.session.commit()
	activate_queue()

def queue_hide_link(token: Token, link_fullnames: list[str]):
	global check_hide_link

	if not link_fullnames:
		return

	check_hide_link = True
	for index in range(0, len(link_fullnames), 50):
		fullnames = ','.join(link_fullnames[index:index+50])
		hide_link = HideLink(oauth_token_id=token.id, link_id=fullnames)
		db.session.add(hide_link)

	db.session.commit()
	activate_queue()
