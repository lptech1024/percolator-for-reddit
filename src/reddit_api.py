from dataclasses import dataclass, is_dataclass
from enum import Enum
import time
from types import UnionType
from typing import Any, get_type_hints, get_origin, get_args

import requests
import requests.auth
from requests.structures import CaseInsensitiveDict

from env_vars import REDDIT_API_CLIENT_ID, REDDIT_API_CLIENT_SECERT, REDDIT_API_USER_AGENT
from models.identity import AccountJson, Account
from models.oauth import Token
from models.listing import ListingJson

REDDIT_API_SCOPE = 'identity read report'

DEFAULT_REQUEST_TIMEOUT = 3.05
HTTP_BASIC_AUTH = requests.auth.HTTPBasicAuth(username=REDDIT_API_CLIENT_ID, password=REDDIT_API_CLIENT_SECERT)
USER_AGENT_HEADER = { 'User-Agent': REDDIT_API_USER_AGENT or '' }

def get_access_token(one_time_code: str, reddit_redirect_uri: str) -> Token:
	time_before_request = int(time.time())
	access_token_uri = 'https://www.reddit.com/api/v1/access_token'
	access_token_params = {
		'grant_type': 'authorization_code',
		'code': one_time_code,
		'redirect_uri': reddit_redirect_uri
	}
	access_token_request = requests.post(
		url=access_token_uri,
		auth=HTTP_BASIC_AUTH,
		headers=USER_AGENT_HEADER,
		data=access_token_params,
		timeout=DEFAULT_REQUEST_TIMEOUT
	)

	access_token_request.raise_for_status()

	access_token_request_json = access_token_request.json()

	access_token = access_token_request_json.get('access_token')
	if not access_token:
		print('Did not receive access token')
		raise RuntimeError
	potential_refresh_token = access_token_request_json.get('refresh_token')
	returned_scope = access_token_request_json.get('scope')
	if set(returned_scope.split(' ')) != set(REDDIT_API_SCOPE.split(' ')):
		print( f'[{returned_scope}] does not match expected scope [{REDDIT_API_SCOPE}]')
		raise RuntimeError
	returned_token_type = access_token_request_json.get('token_type')
	expected_token_type = 'bearer'
	if returned_token_type != expected_token_type:
		print(f'{returned_token_type} does not match expected token type {expected_token_type}')
		raise RuntimeError
	# Expected 2 weeks or less
	# https://www.oauth.com/oauth2-servers/access-tokens/access-token-lifetime/
	# Unix Epoch Seconds
	returned_expires_in = access_token_request_json.get('expires_in')
	if not isinstance(returned_expires_in, int):
		print(f'{returned_expires_in} is not of expected type')
		raise RuntimeError
	access_token_expiry = time_before_request + returned_expires_in

	return Token(access_token=access_token, access_token_expiry_epoch=access_token_expiry, refresh_token=potential_refresh_token)

def refresh_access_token(token: Token):
	"""Refresh expired access token

	Raises HTTPError
	"""
	time_before_request = int(time.time())
	refresh_token_uri = 'https://www.reddit.com/api/v1/access_token'
	refresh_token_params = {
		'grant_type': 'refresh_token',
		'refresh_token': token.refresh_token
	}
	refresh_token_request = requests.post(
		url=refresh_token_uri,
		auth=HTTP_BASIC_AUTH,
		headers=USER_AGENT_HEADER,
		data=refresh_token_params,
		timeout=DEFAULT_REQUEST_TIMEOUT
	)

	refresh_token_request.raise_for_status()

	refresh_token_request_json = refresh_token_request.json()

	access_token = refresh_token_request_json.get('access_token')
	if not access_token:
		print('Did not receive access token')
		raise RuntimeError
	potential_refresh_token = refresh_token_request_json.get('refresh_token')
	returned_scope = refresh_token_request_json.get('scope')
	if set(returned_scope.split(' ')) != set(REDDIT_API_SCOPE.split(' ')):
		print(f'[{returned_scope}] does not match expected scope [{REDDIT_API_SCOPE}]')
		raise RuntimeError
	returned_token_type = refresh_token_request_json.get('token_type')
	expected_token_type = 'bearer'
	if returned_token_type != expected_token_type:
		print(f'{returned_token_type} does not match expected token type {expected_token_type}')
		raise RuntimeError
	# Expected 2 weeks or less
	# https://www.oauth.com/oauth2-servers/access-tokens/access-token-lifetime/
	# Unix Epoch Seconds
	returned_expires_in = refresh_token_request_json.get('expires_in')
	if not isinstance(returned_expires_in, int):
		print(f'{returned_expires_in} is not of expected type')
		raise RuntimeError
	access_token_expiry = time_before_request + returned_expires_in

	return Token(
		access_token = access_token,
		access_token_expiry_epoch=access_token_expiry,
		refresh_token=potential_refresh_token
	)

def get_headers(access_token: str) -> dict[str, str]:
	"""Returns headers for OAuth API methods"""
	headers = dict(USER_AGENT_HEADER)
	headers['Authorization'] = f'bearer {access_token}'
	return headers

def reddit_api_get(
	access_token: str,
	api_path: str,
	params: dict[str, str] | None = None
) -> requests.Response:
	"""Handle API get boilerplate

	api_path: Do not include leading slash
	"""
	if params is None:
		params = { }

	# Unencoded '<', '>', and '&'
	raw_json_params = { 'raw_json': '1' }

	return requests.get(
		url=f'https://oauth.reddit.com/{api_path}',
		headers=get_headers(access_token=access_token),
		params=raw_json_params | params,
		timeout=DEFAULT_REQUEST_TIMEOUT
	)

def reddit_api_post(
	access_token: str,
	api_path: str,
	data: dict[str, str]
) -> requests.Response:
	"""Handle API post boilerplate

	api_path: Do not include leading slash
	"""
	return requests.post(
		url=f'https://oauth.reddit.com/{api_path}',
		headers=get_headers(access_token=access_token),
		data=data,
		timeout=DEFAULT_REQUEST_TIMEOUT
	)

@dataclass
class RateLimit:
	"""1 request per second

	reset in seconds
	"""
	headers = [
		'x-ratelimit-remaining',
		'x-ratelimit-reset'
	]

	# Order must match "headers"
	remaining: float
	reset: int

	def has_reached_limit(self) -> bool:
		"""Check before making API call"""
		return self.remaining < 1

def get_rate_limit(headers: CaseInsensitiveDict[str]) -> RateLimit:
	"""Raises RuntimeError, NotImplementedError"""
	validated_params: list[Any] = []

	rate_limit_types = list(get_type_hints(RateLimit).values())

	for header in RateLimit.headers:
		if header not in headers:
			raise RuntimeError

		value = headers[header]
		# Expected type
		expected_type = rate_limit_types.pop(0)
		if expected_type == int:
			if not value.isdigit():
				raise RuntimeError

			value = int(value)
		elif expected_type == float:
			if not value.replace('.', '', 1).isdigit():
				raise RuntimeError

			value = float(value)
		elif expected_type == str:
			pass
		else:
			raise NotImplementedError

		validated_params.append(value)

	# https://github.com/PyCQA/pylint/issues/5225
	#pylint: disable=no-value-for-parameter
	return RateLimit(*validated_params)
	#pylint: enable=no-value-for-parameter

def reddit_api_transform_json(api_class: Any, json: Any) -> Any:
	"""api_class: Class including expected json names and types

	json: Response JSON to check

	Returns populated class instance
	"""
	validated_params: list[Any] = []
	class_type_hints = get_type_hints(api_class)

	for attribute, attribute_type in class_type_hints.items():
		if attribute not in json:
			raise RuntimeError

		value: Any = json[attribute]

		if issubclass(get_origin(attribute_type) or bool, UnionType):
			found_match = False
			union_types = get_args(attribute_type)
			for union_type in union_types:
				if isinstance(value, union_type):
					found_match = True
					break
			if not found_match:
				raise RuntimeError
		elif issubclass(attribute_type, Enum):
			value = attribute_type(value)
		elif is_dataclass(attribute_type):
			value = reddit_api_transform_json(attribute_type, value)
		elif issubclass(get_origin(attribute_type) or bool, list):
			args = get_args(attribute_type)
			if len(args) != 1 or not is_dataclass(args[0]):
				raise RuntimeError
			items: list[Any] = []
			for item in value:
				items.append(reddit_api_transform_json(args[0], item))
			value = items
		elif not isinstance(value, attribute_type):
			raise RuntimeError

		validated_params.append(value)

	return api_class(*validated_params)

def get_user_identity(token: Token) -> tuple[Account, RateLimit, float, Token | None]:
	"""Scope identity

	https://www.reddit.com/dev/api#GET_api_v1_me
	"""
	access_token: str = token.access_token

	new_token = None
	if time.time() >= token.access_token_expiry_epoch:
		new_token = refresh_access_token(token=token)
		access_token: str = new_token.access_token

	call_start_time = time.monotonic()

	user_identity_request = reddit_api_get(access_token=access_token, api_path='api/v1/me')

	user_identity_request.raise_for_status()

	rate_limit = get_rate_limit(user_identity_request.headers)

	account_json: AccountJson = reddit_api_transform_json(
		api_class=AccountJson,
		json=user_identity_request.json()
	)

	return (
		account_json.get_account(token.id),
		rate_limit,
		call_start_time,
		new_token
	)

def get_listing(token: Token, listing_path: str, before: str | None = None):
	"""Scope read"""
	access_token: str = token.access_token

	new_token = None
	if time.time() >= token.access_token_expiry_epoch:
		new_token = refresh_access_token(token=token)
		access_token: str = new_token.access_token

	# 25 default, 100 max
	listing_params = { 'limit': '100' }
	if before:
		listing_params['before'] = before

	call_start_time = time.monotonic()
	listing_request = reddit_api_get(
		access_token=access_token,
		api_path=listing_path,
		params=listing_params
	)

	listing_request.raise_for_status()

	rate_limit = get_rate_limit(listing_request.headers)

	listing_json = reddit_api_transform_json(api_class=ListingJson, json=listing_request.json())

	return (
		listing_json,
		rate_limit,
		call_start_time,
		new_token
	)

def hide_from_listing(
	token: Token,
	fullnames: str
) -> tuple[RateLimit, float, Token | None]:
	"""scope: report

	https://www.reddit.com/dev/api#POST_api_hide
	"""
	access_token: str = token.access_token

	new_token = None
	if time.time() >= token.access_token_expiry_epoch:
		new_token = refresh_access_token(token=token)
		access_token: str = new_token.access_token

	fullname_data = { 'id': fullnames }

	call_start_time = time.monotonic()
	hide_request = reddit_api_post(access_token=access_token, api_path='api/hide', data=fullname_data)

	hide_request.raise_for_status()

	return (get_rate_limit(hide_request.headers), call_start_time, new_token)
