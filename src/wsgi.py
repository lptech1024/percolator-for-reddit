from uuid import uuid4, UUID

from flask import Flask, render_template, redirect, request, url_for, session
from sqlalchemy import select
import urllib.parse

from env_vars import SQLALCHEMY_DATABASE_URI, REDDIT_API_CLIENT_ID, SERVICE_URI, REDDIT_REDIRECT_PATH, FLASK_SECRET_KEY
from reddit_api import get_access_token, REDDIT_API_SCOPE
from reddit_queue import activate_queue, queue_get_user_identity, queue_filter_listing
from models.common import db
from models.local_user import Session
from models.oauth import AuthorizationRequest, Token
from models.filter import Rule

REDDIT_REDIRECT_URI = f'{SERVICE_URI}/{REDDIT_REDIRECT_PATH}'

app = Flask(__name__)
# Address warning
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SECRET_KEY'] = FLASK_SECRET_KEY
db.init_app(app)

@app.get('/')
def home():
	skip_session = request.args.get('skip_session', 'false', str)
	potential_key_value = session.get('key')
	if skip_session.lower() != 'true' and potential_key_value is not None:
		potential_key = db.session.get(Session, potential_key_value)
		if potential_key is not None:
			return redirect('/dashboard')

	accounts = [token.account for token in db.session.scalars(select(Token)).all()]
	return render_template(
		'home.html',
		accounts=accounts
	)

@app.route('/switch_user/<int:oauth_token_id>')
def switch_user(oauth_token_id):
	token: Token | None = db.session.get(Token, oauth_token_id)
	if not token:
		return f'Bad id {oauth_token_id}'

	potential_key_value = session.get('key')
	if potential_key_value is None:
		if token.session:
			db.session.delete(token.session)
			db.session.flush()
		new_session = Session(oauth_token_id=token.id)
		db.session.add(new_session)
		db.session.commit()

		session['key'] = new_session.id
		return redirect('/dashboard')

	potential_key: Session | None = db.session.get(Session, potential_key_value)
	if potential_key is None:
		if token.session:
			db.session.delete(token.session)
			db.session.flush()
		new_session = Session(oauth_token_id=token.id)
		db.session.add(new_session)
		db.session.commit()

		session['key'] = new_session.id
		return redirect('/dashboard')

	if potential_key.oauth_token_id == oauth_token_id:
		return redirect('/dashboard')

	if token.session:
		db.session.delete(token.session)
		db.session.flush()
	potential_key.oauth_token_id = token.id
	db.session.commit()
	return redirect('/dashboard')

@app.get('/dashboard')
def dashboard():
	potential_key_value = session.get('key')
	if potential_key_value is None:
		return redirect(url_for('home'))

	potential_key: Session | None = db.session.get(Session, potential_key_value)
	if potential_key is None:
		return redirect(url_for('home', skip_session='true'))

	rules = potential_key.token.account.rules

	return render_template('dashboard.html.j2', filter_rules=rules)

@app.get('/filter')
def run_filters():
	potential_key_value = session.get('key')
	if potential_key_value is None:
		return 'Invalid session', 401

	potential_key: Session | None = db.session.get(Session, potential_key_value)
	if potential_key is None:
		return 'Invalid session', 401

	queue_filter_listing(potential_key.token, '', 150)
	return '', 200

@app.post('/filter')
def add_filter_rule():
	potential_key_value = session.get('key')
	if potential_key_value is None:
		return 'Invalid session', 401

	potential_key: Session | None = db.session.get(Session, potential_key_value)
	if potential_key is None:
		return 'Invalid session', 401

	data: dict[str, str] = request.get_json()
	if 'filter_text' not in data:
		return 'key [filter_text] missing or invalid', 400

	rule = Rule(match=data['filter_text'], account_id=potential_key.token.account.id)
	db.session.add(rule)
	db.session.commit()

	return render_template('filter_rule.html.j2', filter_rules=[rule])

@app.delete('/filter/<uuid:filter_id>')
def delete_filter_rule(filter_id: UUID):
	potential_key_value = session.get('key')
	if potential_key_value is None:
		return 'Invalid session', 401

	potential_key: Session | None = db.session.get(Session, potential_key_value)
	if potential_key is None:
		return 'Invalid session', 401

	rule = db.session.get(Rule, str(filter_id))
	if not rule:
		return f'Rule [{filter_id}] not found', 404

	db.session.delete(rule)
	db.session.commit()

	return '', 204

@app.get(f'/{REDDIT_REDIRECT_PATH}')
def handle_reddit_authorization():
	error = request.args.get(key='error', type=str)
	match error:
		case None:
			pass
		case 'access_denied':
			return 'Access denied'
		case 'unsupported_response_type':
			return 'Type must be \'code\''
		case 'invalid_scope':
			return 'Invalid scope'
		case 'invalid_request':
			return 'Check request parameters'
		case _:
			return f'Unknown error: {error}'

	state = request.args.get(key='state', type=str)
	if not state:
		return 'Missing state parameter'
	authorization_request: AuthorizationRequest | None = db.session.get(AuthorizationRequest, state)
	if not authorization_request:
		return f'Bad authorization request id [{state}]'

	db.session.delete(authorization_request)
	db.session.commit()

	code = request.args.get(key='code', type=str)
	if not code:
		return f'Missing one-time code'

	token: Token = get_access_token(one_time_code=code, reddit_redirect_uri=REDDIT_REDIRECT_URI)
	db.session.add(token)
	# Commit needed to generate id
	db.session.commit()

	queue_get_user_identity(token=token)

	return f'Welcome, {user_identity.username}!'

@app.get('/request_reddit_authorization')
def prompt_user_authorization():
	unique_auth_req_id = str(uuid4())
	authorization_request = AuthorizationRequest(state=unique_auth_req_id)
	db.session.add(authorization_request)
	db.session.commit()

	authorize_path = 'https://www.reddit.com/api/v1/authorize?'
	authorize_params = {
		'client_id': REDDIT_API_CLIENT_ID,
		'response_type': 'code',
		'redirect_uri': REDDIT_REDIRECT_URI,
		'state': unique_auth_req_id,
		'duration': 'permanent',
		'scope': REDDIT_API_SCOPE
	}

	return redirect(authorize_path + urllib.parse.urlencode(authorize_params))
