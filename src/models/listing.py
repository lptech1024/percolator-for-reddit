from dataclasses import dataclass
from enum import Enum

from .filter import Rule

class SubredditType(Enum):
	"""https://www.reddit.com/r/redditdev/comments/1kthy1/api_change_10_new_attributes_available_on/"""
	PUBLIC = 'public'
	PRIVATE = 'private'
	RESTRICTED = 'restricted'
	GOLD_RESTRICTED = 'gold_restricted'
	ARCHIVED = 'archived'

class Kind(Enum):
	"""Does not include trailing underscore"""
	COMMENT = 't1'
	ACCOUNT = 't2'
	LINK = 't3'
	MESSAGE = 't4'
	SUBREDDIT = 't5'
	AWARD = 't6'

@dataclass
class LinkJson: #pylint: disable=too-many-instance-attributes
	"""selftext: body

	Partial implementation
	"""
	# Optional - deleted
	# author_fullname: str
	author_is_blocked: bool
	clicked: bool
	hidden: bool
	id: str
	selftext: str
	subreddit: str
	subreddit_id: str
	subreddit_type: SubredditType
	title: str
	visited: bool

@dataclass
class ListingDataChildrenJson:
	"""Full implementation"""
	kind: Kind
	data: LinkJson

@dataclass
class ListingDataJson:
	"""after or before: link fullname

	Partial implementation

	https://www.reddit.com/dev/api#fullnames
	"""
	before: str | None
	after: str | None
	children: list[ListingDataChildrenJson]

@dataclass
class ListingJson:
	"""kind: expected 'Listing'

	Full implementation

	https://www.reddit.com/dev/api#listings"""
	kind: str
	data: ListingDataJson

def get_fullname(listing_data_children: ListingDataChildrenJson) -> str:
	'''https://www.reddit.com/dev/api#fullnames'''
	return f'{listing_data_children.kind.value}_{listing_data_children.data.id}'

def get_matched_listings(listing_json: ListingJson, rules: list[Rule]):
	matched_fullnames: list[str] = []

	filter_rules_normalized: tuple[str] = tuple(rule.match.lower() for rule in rules)

	for link in listing_json.data.children:
		for unwanted_text in filter_rules_normalized:
			if (
				unwanted_text in link.data.title.lower() or
				unwanted_text in link.data.selftext.lower()
			):
				matched_fullnames.append(get_fullname(link))

	return matched_fullnames
