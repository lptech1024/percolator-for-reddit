from sqlalchemy import Column, TIMESTAMP, String, BigInteger, Integer, Identity
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .common import db

class AuthorizationRequest(db.Model):
	__tablename__ = 'oauth_authorization_request'

	state = Column(String, nullable=False, primary_key=True)
	created_date = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())

class Token(db.Model):
	__tablename__ = 'oauth_token'

	id = Column(Integer, Identity(start=1, cycle=False), primary_key=True)
	access_token = Column(String, unique=True)
	# Account for Year 2038 problem
	access_token_expiry_epoch = Column(BigInteger)
	refresh_token = Column(String, unique=True, nullable=False)
	created_date = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
	account = relationship('Account', back_populates='token', cascade='all, delete', passive_deletes=True, uselist=False)
	session = relationship('Session', back_populates='token', passive_deletes=True, uselist=False)
	get_user_identity = relationship('GetUserIdentity', back_populates='token', cascade='all, delete', passive_deletes=True)
	filter_listing = relationship('FilterListing', back_populates='token', cascade='all, delete', passive_deletes=True)
	hide_link = relationship('HideLink', back_populates='token', cascade='all, delete', passive_deletes=True)

	def update(self, token: 'Token'):
		'''Updates access token details'''
		self.access_token = token.access_token
		self.access_token_expiry_epoch = token.access_token_expiry_epoch
