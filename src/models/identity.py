from dataclasses import dataclass

from sqlalchemy import Column, Integer, Identity, ForeignKey, String, TIMESTAMP
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .common import db

class Account(db.Model):
	__tablename__ = 'identity_account'

	id = Column(Integer, Identity(start=1, cycle=False), primary_key=True)
	oauth_token_id = Column(Integer, ForeignKey('oauth_token.id', ondelete='CASCADE'), nullable=False, unique=True)
	username = Column(String, nullable=False)
	user_id = Column(String, nullable=False)
	created_date = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
	token = relationship('Token', back_populates='account')
	rules = relationship('Rule', back_populates='account', cascade='all, delete', passive_deletes=True)

@dataclass
class AccountJson():
	def get_account(self, oauth_token_id: int) -> Account:
		return Account(oauth_token_id=oauth_token_id, username=self.name, user_id=self.id)

	id: str
	name: str
