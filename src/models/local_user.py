from sqlalchemy import Column, TIMESTAMP, Integer, ForeignKey, text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .common import db

class Session(db.Model):
	__tablename__ = 'local_user_session'

	id = Column(UUID, primary_key=True, server_default=text('gen_random_uuid()'))
	oauth_token_id = Column(Integer, ForeignKey('oauth_token.id', ondelete='SET NULL'), nullable=True, unique=True)
	last_login = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
	login_count = Column(Integer, server_default=text('1'))
	created_date = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
	token = relationship('Token', back_populates='session')
