from sqlalchemy import Column, TIMESTAMP, ForeignKey, text, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func

from .common import db

class GetUserIdentity(db.Model):
	__tablename__ = 'task_get_user_identity'

	id = Column(UUID, primary_key=True, server_default=text('gen_random_uuid()'))
	oauth_token_id = Column(Integer, ForeignKey('oauth_token.id', ondelete='CASCADE'), nullable=False)
	created_date = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
	token = relationship('Token', back_populates='get_user_identity')

class FilterListing(db.Model):
	__tablename__ = 'task_filter_listing'

	id = Column(UUID, primary_key=True, server_default=text('gen_random_uuid()'))
	oauth_token_id = Column(Integer, ForeignKey('oauth_token.id', ondelete='CASCADE'), nullable=False)
	url = Column(String, nullable=False)
	before = Column(String)
	remaining_count = Column(Integer, nullable=False)
	created_date = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
	token = relationship('Token', back_populates='filter_listing')

class HideLink(db.Model):
	__tablename__ = 'task_hide_link'

	id = Column(UUID, primary_key=True, server_default=text('gen_random_uuid()'))
	oauth_token_id = Column(Integer, ForeignKey('oauth_token.id', ondelete='CASCADE'), nullable=False)
	link_id = Column(String, nullable=False)
	created_date = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
	token = relationship('Token', back_populates='hide_link')
