from sqlalchemy import Column, TIMESTAMP, ForeignKey, text, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func

from .common import db

class Rule(db.Model):
	__tablename__ = 'filter_rule'

	id = Column(UUID, primary_key=True, server_default=text('gen_random_uuid()'))
	match = Column(String, nullable=False)
	account_id = Column(Integer, ForeignKey('identity_account.id', ondelete='CASCADE'), nullable=False)
	created_date = Column(TIMESTAMP(timezone=True), nullable=False, server_default=func.now())
	account = relationship('Account', back_populates='rules')
