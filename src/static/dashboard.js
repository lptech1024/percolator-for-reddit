async function sendFormData(url, formData) {
	const formDataObj = Object.fromEntries(formData.entries())

	const response = await fetch(
		url,
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'text/html'
			},
			body: JSON.stringify(formDataObj)
		}
	);
	if (!response.ok) {
		const message = await response.text();
		throw new Error(message);
	}

	return response.text();
};

async function handleFormSubmit(event) {
	event.preventDefault();

	const form = event.currentTarget;
	const url = form.action;

	const formData = new FormData(form);

	try {
		const filterRule = await sendFormData(url, formData);
		const filters = document.getElementById('filters');
		filters.insertAdjacentHTML('beforeend', filterRule)
	}
	catch (error) {
		console.error(error);
	}
};

const newFilter = document.getElementById('new-filter');
newFilter.addEventListener('submit', handleFormSubmit);

async function sendDelete(url, id) {
	const response = await fetch(
		`${url}/${id}`,
		{ method: 'DELETE' }
	);
	if (!response.ok) {
		const message = await response.text();
		throw new Error(message);
	}
}

async function handleListItemDelete(event) {
	if (!(event.target instanceof HTMLButtonElement)) {
		return;
	}

	const url = event.currentTarget.dataset.href;
	const id = event.target.dataset.id;

	try {
		await sendDelete(url, id)
		event.target.parentElement.remove();
	}
	catch (error) {
		console.error(error);
	}
};

const filters = document.getElementById('filters');
filters.addEventListener('click', handleListItemDelete);

async function sendGet(url) {
	const response = await fetch(
		url,
		{ method: 'GET' }
	);
	if (!response.ok) {
		const message = await response.text();
		throw new Error(message);
	}
};

async function handleSimpleGet(event) {
	const url = event.target.dataset.href;

	try {
		await sendGet(url);
	}
	catch (error) {
		console.error(error);
	}
};

const filterFrontPage = document.getElementById('filter-front-page')
filterFrontPage.addEventListener('click', handleSimpleGet)
