"""Add tables for AuthorizationRequest and Token

Revision ID: 37429d005b3e
Revises: 
Create Date: 2023-02-13 01:37:41.244283+00:00

"""
from alembic import op
import sqlalchemy as sa


revision = '37429d005b3e'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('oauth_authorization_request',
    sa.Column('state', sa.String(), nullable=False),
    sa.Column('created_date', sa.TIMESTAMP(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.PrimaryKeyConstraint('state'),
    sa.UniqueConstraint('state')
    )
    op.create_table('oauth_token',
    sa.Column('id', sa.Integer(), sa.Identity(always=False, start=1, cycle=False), nullable=False),
    sa.Column('access_token', sa.String(), nullable=True),
    sa.Column('access_token_expiry_epoch', sa.BigInteger(), nullable=True),
    sa.Column('refresh_token', sa.String(), nullable=False),
    sa.Column('created_date', sa.TIMESTAMP(timezone=True), server_default=sa.text('now()'), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('access_token'),
    sa.UniqueConstraint('refresh_token')
    )


def downgrade():
    op.drop_table('oauth_token')
    op.drop_table('oauth_authorization_request')
