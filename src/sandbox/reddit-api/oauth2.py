"""Reddit OAuth2 proof of concept"""
import configparser
import sys
import time
import webbrowser
import urllib.parse
import http.server
import socketserver
from uuid import uuid4
from http import HTTPStatus
from os.path import exists
from dataclasses import dataclass, is_dataclass
from enum import Enum
from typing import NamedTuple, Any, ClassVar, get_type_hints, get_origin, get_args
from types import UnionType
from requests.structures import CaseInsensitiveDict
import requests.auth
import requests

# https://github.com/reddit-archive/reddit/wiki/OAuth2

class OAuthAuthorizationDuration(Enum):
	"""Choose TEMPORARY if you're completing a one-time request"""
	TEMPORARY = 'temporary'
	PERMANENT = 'permanent'

class Kind(Enum):
	"""Does not include trailing underscore"""
	COMMENT = 't1'
	ACCOUNT = 't2'
	LINK = 't3'
	MESSAGE = 't4'
	SUBREDDIT = 't5'
	AWARD = 't6'

class SubredditType(Enum):
	"""https://www.reddit.com/r/redditdev/comments/1kthy1/api_change_10_new_attributes_available_on/"""
	PUBLIC = 'public'
	PRIVATE = 'private'
	RESTRICTED = 'restricted'
	GOLD_RESTRICTED = 'gold_restricted'
	ARCHIVED = 'archived'

@dataclass
class ClientAuthorization:
	"""App authorization"""
	client_id: str
	client_secret: str

	@property
	def http_basic_auth(self) -> requests.auth.HTTPBasicAuth:
		"""Used for Requests HTTP Library"""
		return requests.auth.HTTPBasicAuth(username=self.client_id, password=self.client_secret)

@dataclass
class TokenDetails:
	"""OAuth Token"""
	default_access_token: ClassVar[None] = None
	default_access_token_expiry_epoch: ClassVar[int] = 0

	access_token: str | None = default_access_token
	refresh_token: str | None = None
	access_token_expiry_epoch: int = default_access_token_expiry_epoch

	def has_access_token_expired(self) -> bool:
		"""Returns True if access token expired"""
		return time.time() > self.access_token_expiry_epoch

	def has_valid_access_token(self) -> bool:
		"""Returns True if access token can be used"""
		return bool(self.access_token and not self.has_access_token_expired())

	def need_refresh_access_token(self) -> bool:
		"""Returns True if invalid access token and can be refreshed"""
		return bool((not self.access_token or self.has_access_token_expired()) and self.refresh_token)

	def reset_access_token(self) -> None:
		"""Set access token and its expiry to defaults"""
		self.access_token = TokenDetails.default_access_token
		self.access_token_expiry_epoch = TokenDetails.default_access_token_expiry_epoch

@dataclass
class OAuthProcessParameters:
	"""Common parameters used in the OAuth process

	scope:
		space-separated

		identity, edit, flair, history, modconfig, modflair, modlog, modposts, modwiki, mysubreddits,
		privatemessages, read, report, save, submit, subscribe, vote, wikiedit, wikiread

	duration: "temporary" or "permanent"
	"""
	client_authorization: ClientAuthorization
	user_agent: str
	socketserver_allow_reuse_address: bool
	redirect_uri: str
	scope: str
	token: TokenDetails
	duration: OAuthAuthorizationDuration = OAuthAuthorizationDuration.TEMPORARY

	@property
	def authorization_header(self) -> str:
		"""Derived from access token

		Raises RuntimeError if no access token

		Does not account for access token expiration
		"""
		if not self.token.access_token:
			raise RuntimeError

		return f'bearer {self.token.access_token}'

class HttpHandler(http.server.SimpleHTTPRequestHandler):
	"""Handle Reddit oauth authorization"""
	oauth_parameters: OAuthProcessParameters
	unique_auth_req_id: str = str(uuid4())
	def do_GET(self) -> None:
		self.send_response(HTTPStatus.OK)
		self.end_headers()

		# '/?id=arst'
		query_params = urllib.parse.parse_qs(self.path[2:])
		response = None
		query_value_error = get_single_qs_value(query_params, 'error')
		match query_value_error:
			case None:
				pass
			case 'access_denied':
				response = 'Access denied'
			case 'unsupported_response_type':
				response = 'Type must be \'code\''
			case 'invalid_scope':
				response = 'Invalid scope'
			case 'invalid_request':
				response = 'Check request parameters'
			case _:
				response = f'Unknown error: {query_value_error}'

		query_value_state = get_single_qs_value(query_params, 'state')
		if response is None and query_value_state != HttpHandler.unique_auth_req_id:
			response = (
				'Bad authorization request id\n\n'
				f'{query_value_state} != {HttpHandler.unique_auth_req_id}'
			)

		if response is None:
			code_default = 'Missing code'
			response = get_single_qs_value(query_params, 'code', code_default)
			if response != code_default:
				(
					HttpHandler.oauth_parameters.token.access_token,
					HttpHandler.oauth_parameters.token.access_token_expiry_epoch,
					HttpHandler.oauth_parameters.token.refresh_token
				) = get_access_token(
					one_time_code=str(response),
					oauth_parameters=HttpHandler.oauth_parameters
				)
				check_update_config(oauth_parameters=HttpHandler.oauth_parameters)
				filter_front_page(HttpHandler.oauth_parameters)
				return

		self.wfile.write(response.encode())

@dataclass
class RateLimit:
	"""1 request per second

	reset in seconds
	"""
	headers = [
		'x-ratelimit-remaining',
		'x-ratelimit-reset'
	]

	# Order must match "headers"
	remaining: float
	reset: int

	def has_reached_limit(self) -> bool:
		"""Check before making API call"""
		return self.remaining < 1

@dataclass
class Link: #pylint: disable=too-many-instance-attributes
	"""selftext: body

	Partial implementation
	"""
	author_fullname: str
	author_is_blocked: bool
	clicked: bool
	hidden: bool
	id: str
	selftext: str
	subreddit: str
	subreddit_id: str
	subreddit_type: SubredditType
	title: str
	visited: bool

@dataclass
class ListingDataChildren:
	"""Full implementation"""
	kind: Kind
	data: Link

@dataclass
class ListingData:
	"""after or before: link fullname

	Partial implementation

	https://www.reddit.com/dev/api#fullnames
	"""
	before: str | None
	after: str | None
	children: list[ListingDataChildren]

@dataclass
class Listing:
	"""kind: expected 'Listing'

	Full implementation

	https://www.reddit.com/dev/api#listings"""
	kind: str
	data: ListingData

def get_fullname(listing_data_children: ListingDataChildren) -> str:
	'''https://www.reddit.com/dev/api#fullnames'''
	return f'{listing_data_children.kind.value}_{listing_data_children.data.id}'

def filter_front_page(oauth_parameters: OAuthProcessParameters) -> None:
	"""Revokes access token if listings are hidden"""
	if oauth_parameters.token.has_access_token_expired():
		print('We ran out of time to make additional API calls')
		return

	matched_listings: list[ListingDataChildren] = []
	before: str | None = None
	parsed_count = 0
	call_time = 0

	# ~10 seconds, ~40 pages
	while parsed_count < 1000:
		# 5 KB/link typical
		if oauth_parameters.token.has_access_token_expired():
			print('We ran out of time to make additional API calls')
			return

		(listing, rate_limit, call_time) = get_front_page(oauth_parameters, before)

		if not listing or not listing.data or not listing.data.children:
			break

		matched_listings += filter_listing(listing)
		parsed_count += len(listing.data.children)

		before = get_fullname(listing.data.children[-1])

		if rate_limit.has_reached_limit():
			print('Rate limit exceeded')
			return
		if call_time > time.monotonic():
			print('Call was made in the future')
			return
		while (time.monotonic() - call_time) < 1:
			pass

	if not matched_listings:
		print('No matched listings')
		return

	for index in range(0, len(matched_listings), 50):
		while (time.monotonic() - call_time) < 1:
			pass

		if oauth_parameters.token.has_access_token_expired():
			print('We ran out of time to make additional API calls')
			return

		rate_limit, call_time = hide_from_listing(
			oauth_params=oauth_parameters,
			links=matched_listings[index:index+50]
		)

		if rate_limit.has_reached_limit():
			print('Rate limit exceeded')
			return
		if call_time > time.monotonic():
			print('Call was made in the future')
			return

	revoke_access_token(oauth_parameters=oauth_parameters)

def get_single_qs_value(
	query_params: dict[str, list[str]],
	query_string: str,
	default: str | None = None
) -> str | None:
	"""Function returns querystring value if available"""
	if query_params is None or query_string is None:
		return default

	query_values = query_params.get(query_string)
	if not query_values:
		return default
	if len(query_values) > 1:
		print(f'{query_string} has {len(query_values)} values')
		return default

	return query_values[0] or default

def read_filter_rules() -> list[str]:
	"""Returns list of potential text to filter out posts"""
	rules: list[str] = []

	path='reddit-filter'
	if not exists(path):
		path = f'src/sandbox/reddit-api/{path}'
		if not exists(path):
			print(f'Can\'t find [{path}]')
			return rules
	with open(path, encoding='UTF-8') as file:
		while (match_text := file.readline().rstrip()):
			rules.append(match_text)

	return rules

def read_config() -> configparser.ConfigParser:
	"""Returns Reddit API config"""
	config = configparser.ConfigParser()
	path = 'reddit-api.ini'
	if not exists(path):
		path = f'src/sandbox/reddit-api/{path}'
		if not exists(path):
			print(f'Can\'t find [{path}]')
			sys.exit()
	config.read(path)
	return config

def write_config(config: configparser.ConfigParser) -> bool:
	"""Write Redit API config. Returns success"""
	path = 'reddit-api.ini'
	if not exists(path):
		path = f'src/sandbox/reddit-api/{path}'
		if not exists(path):
			print(f'Can\'t find [{path}]')
			return False
	with open(path, 'w', encoding='utf-8') as config_file:
		config.write(config_file)
		return True

def check_update_config(oauth_parameters: OAuthProcessParameters) -> bool:
	"""Updates config token details if needed

	Returns True if config matches in-memory state
	"""
	config = read_config()

	class ConfigField(NamedTuple):
		"""Field in a section

		value: used as string version
		"""
		key: str
		value: Any

	class ConfigSection(NamedTuple):
		"""Section with entries"""
		section: str
		entries: tuple[ConfigField, ...]

	configs_to_check = ConfigSection(
		'Token',
		 (
			ConfigField('AccessToken', oauth_parameters.token.access_token),
			ConfigField('AccessTokenExpiryEpoch', oauth_parameters.token.access_token_expiry_epoch),
			ConfigField('RefreshToken', oauth_parameters.token.refresh_token)
		 )
	)

	config_needs_update = False
	if configs_to_check.section not in config:
		config_needs_update = True
		config[configs_to_check.section] = {}

	for config_field in configs_to_check.entries:
		stored_value = config[configs_to_check.section].get(config_field.key)
		# Account for None
		if config_field.value != stored_value != str(config_field.value):
			config_needs_update = True
			if config_field.value is None:
				config[configs_to_check.section].pop(config_field.key, None)
			else:
				config[configs_to_check.section][config_field.key] = str(config_field.value)

	return write_config(config=config) if config_needs_update else True

def get_config() -> tuple[tuple[str, str, str, bool], tuple[str | None, int, str | None]]:
	"""Read OAuth2 customizations"""
	config = read_config()
	# We may not have a token yet
	potential_access_token = None
	potential_access_token_expiry_epoch = 0
	potential_refresh_token = None

	access_token_section = 'Token'
	if access_token_section in config:
		potential_access_token = config[access_token_section].get('AccessToken')
		potential_access_token_expiry_epoch = config.getint(
			access_token_section,
			'AccessTokenExpiryEpoch'
		)
		potential_refresh_token = config[access_token_section].get('RefreshToken')
	config_section = 'Config'
	try:
		return (
			(
				config[config_section]['ClientId'],
				config[config_section]['ClientSecret'],
				config[config_section]['UserAgent'],
				config.getboolean(
					config_section,
					'SocketServerAllowReuseAddress',
					fallback=socketserver.TCPServer.allow_reuse_address
				)
			),
			(
				potential_access_token,
				potential_access_token_expiry_epoch,
				potential_refresh_token
			)
		)
	except (configparser.NoSectionError, configparser.MissingSectionHeaderError):
		print(f'Missing section \'{config_section}\'')
		sys.exit()
	except configparser.Error:
		print('Config error')
		sys.exit()

def start_server(
	local_port: int,
	oauth_parameters: OAuthProcessParameters,
	unique_auth_req_id: str
) -> None:
	"""Handle TCP requests"""
	# Unintentionally doesn't allow different instances
	HttpHandler.oauth_parameters = oauth_parameters
	HttpHandler.unique_auth_req_id = unique_auth_req_id
	# Potentially allow port reuse during time-wait
	# https://www.ietf.org/rfc/rfc793.txt
	socketserver.TCPServer.allow_reuse_address = oauth_parameters.socketserver_allow_reuse_address
	# Await response after manual user action
	socketserver.TCPServer.timeout = None
	with socketserver.TCPServer(('', local_port), HttpHandler) as httpd:
		httpd.handle_request()

def prompt_user_authorization(
	oauth_parameters: OAuthProcessParameters,
	unique_auth_req_id: str
) -> None:
	"""Prompt user for oauth authorization via constructed URL"""

	authorize_path = 'https://www.reddit.com/api/v1/authorize?'
	authorize_params = {
		'client_id': oauth_parameters.client_authorization.client_id,
		'response_type': 'code',
		'redirect_uri': oauth_parameters.redirect_uri,
		'state': unique_auth_req_id,
		'duration': oauth_parameters.duration.value,
		'scope': oauth_parameters.scope
	}

	webbrowser.open(authorize_path + urllib.parse.urlencode(authorize_params))

def get_user_agent_header(oauth_params: OAuthProcessParameters) -> dict[str, str]:
	"""Returns ready-to-use user agent header"""
	return {'User-Agent': oauth_params.user_agent}

def get_headers(oauth_params: OAuthProcessParameters) -> dict[str, str]:
	"""Returns headers for OAuth API methods"""
	return {
		'Authorization': oauth_params.authorization_header,
		'User-Agent': oauth_params.user_agent
	}

@dataclass
class UserIdentity():
	"""Interesting API identity parameters"""

	id: str
	name: str
	# is_suspended: bool
	# created_utc: int
	# gold_expiration: str | None

def default_request_timeout():
	"""https://requests.readthedocs.io/en/latest/user/advanced/#timeouts"""
	return 3.05

def reddit_api_get(
	oauth_params: OAuthProcessParameters,
	api_path: str,
	params: dict[str, str] | None = None
) -> requests.Response:
	"""Handle API get boilerplate

	api_path: Do not include leading slash
	"""
	if params is None:
		params = { }

	# Unencoded '<', '>', and '&'
	raw_json_params = { 'raw_json': '1' }

	return requests.get(
		url=f'https://oauth.reddit.com/{api_path}',
		headers=get_headers(oauth_params),
		params=raw_json_params | params,
		timeout=default_request_timeout()
	)

def reddit_api_post(
	oauth_params: OAuthProcessParameters,
	api_path: str,
	data: dict[str, str]
) -> requests.Response:
	"""Handle API post boilerplate

	api_path: Do not include leading slash
	"""
	return requests.post(
		url=f'https://oauth.reddit.com/{api_path}',
		headers=get_headers(oauth_params),
		data=data,
		timeout=default_request_timeout()
	)

def reddit_api_transform_json(api_class: Any, json: Any) -> Any:
	"""api_class: Class including expected json names and types

	json: Response JSON to check

	Returns populated class instance
	"""
	validated_params: list[Any] = []
	class_type_hints = get_type_hints(api_class)

	for attribute, attribute_type in class_type_hints.items():
		if attribute not in json:
			raise RuntimeError

		value: Any = json[attribute]

		if issubclass(get_origin(attribute_type) or bool, UnionType):
			found_match = False
			union_types = get_args(attribute_type)
			for union_type in union_types:
				if isinstance(value, union_type):
					found_match = True
					break
			if not found_match:
				raise RuntimeError
		elif issubclass(attribute_type, Enum):
			value = attribute_type(value)
		elif is_dataclass(attribute_type):
			value = reddit_api_transform_json(attribute_type, value)
		elif issubclass(get_origin(attribute_type) or bool, list):
			args = get_args(attribute_type)
			if len(args) != 1 or not is_dataclass(args[0]):
				raise RuntimeError
			items: list[Any] = []
			for item in value:
				items.append(reddit_api_transform_json(args[0], item))
			value = items
		elif not isinstance(value, attribute_type):
			raise RuntimeError

		validated_params.append(value)

	return api_class(*validated_params)

def get_rate_limit(headers: CaseInsensitiveDict[str]) -> RateLimit:
	"""Raises RuntimeError, NotImplementedError"""
	validated_params: list[Any] = []

	rate_limit_types = list(get_type_hints(RateLimit).values())

	for header in RateLimit.headers:
		if header not in headers:
			raise RuntimeError

		value = headers[header]
		# Expected type
		expected_type = rate_limit_types.pop(0)
		if expected_type == int:
			if not value.isdigit():
				raise RuntimeError

			value = int(value)
		elif expected_type == float:
			if not value.replace('.', '', 1).isdigit():
				raise RuntimeError

			value = float(value)
		elif expected_type == str:
			pass
		else:
			raise NotImplementedError

		validated_params.append(value)

	# https://github.com/PyCQA/pylint/issues/5225
	#pylint: disable=no-value-for-parameter
	return RateLimit(*validated_params)
	#pylint: enable=no-value-for-parameter

def get_user_identity(
	oauth_params: OAuthProcessParameters
) -> tuple[UserIdentity, RateLimit, float]:
	"""Access token must be valid

	Scope identity

	https://www.reddit.com/dev/api#GET_api_v1_me
	"""
	call_start_time = time.monotonic()

	user_identity_request = reddit_api_get(oauth_params=oauth_params, api_path='api/v1/me')

	user_identity_request.raise_for_status()

	rate_limit = get_rate_limit(user_identity_request.headers)

	user_identity: UserIdentity = reddit_api_transform_json(
		api_class=UserIdentity,
		json=user_identity_request.json()
	)

	return (
		user_identity,
		rate_limit,
		call_start_time
	)

def get_front_page(
	oauth_params: OAuthProcessParameters,
	before: str | None = None
) -> tuple[Listing, RateLimit, float]:
	"""Scope read"""
	# 25 default, 100 max
	front_page_params = { 'limit': '100' }
	if before:
		front_page_params['before'] = before

	call_start_time = time.monotonic()
	front_page_request = reddit_api_get(
		oauth_params=oauth_params,
		api_path='',
		params=front_page_params
	)

	front_page_request.raise_for_status()

	rate_limit = get_rate_limit(front_page_request.headers)

	front_page = reddit_api_transform_json(api_class=Listing, json=front_page_request.json())

	return (
		front_page,
		rate_limit,
		call_start_time
	)

def hide_from_listing(
	oauth_params: OAuthProcessParameters,
	links: list[ListingDataChildren]
) -> tuple[RateLimit, float]:
	"""scope: report

	https://www.reddit.com/dev/api#POST_api_hide
	"""
	fullnames = tuple(get_fullname(link) for link in links)
	fullname_data = { 'id': ','.join(fullnames) }

	call_start_time = time.monotonic()
	hide_request = reddit_api_post(oauth_params=oauth_params, api_path='api/hide', data=fullname_data)

	hide_request.raise_for_status()

	return (get_rate_limit(hide_request.headers), call_start_time)

def filter_listing(listing: Listing) -> list[ListingDataChildren]:
	"""Returns matched content"""
	matched_links: list[ListingDataChildren] = []
	filter_rules = read_filter_rules()
	if not filter_rules:
		return matched_links

	filter_rules_normalized = tuple(filter_rule.lower() for filter_rule in filter_rules)

	for link in listing.data.children:
		for unwanted_text in filter_rules_normalized:
			if (
				unwanted_text in link.data.title.lower() or
				unwanted_text in link.data.selftext.lower()
			):
				matched_links.append(link)

	return matched_links

def revoke_access_token(oauth_parameters: OAuthProcessParameters) -> bool:
	"""Revokes access token"""
	revoke_token_uri = 'https://www.reddit.com/api/v1/revoke_token'

	revoke_params = {
		'token_type_hint': 'access_token',
		'token': oauth_parameters.token.access_token
	}

	# Revoking before writing config, which may fail
	revoke_token_request = requests.post(
		url=revoke_token_uri,
		auth=oauth_parameters.client_authorization.http_basic_auth,
		headers=get_user_agent_header(oauth_params=oauth_parameters),
		data=revoke_params,
		timeout=default_request_timeout()
	)

	# RFC 7009 - success even for bad token
	# 401 for invalid credentials
	# 200 observed - archived documentation claims 204
	revoke_token_request.raise_for_status()

	oauth_parameters.token.reset_access_token()
	return check_update_config(oauth_parameters=oauth_parameters)

def refresh_access_token(oauth_parameters: OAuthProcessParameters) -> tuple[str, int, str]:
	"""Refresh expired access token

	Raises HTTPError
	"""
	time_before_request = int(time.time())
	refresh_token_uri = 'https://www.reddit.com/api/v1/access_token'
	refresh_token_params = {
		'grant_type': 'refresh_token',
		'refresh_token': oauth_parameters.token.refresh_token
	}
	refresh_token_request = requests.post(
		url=refresh_token_uri,
		auth=oauth_parameters.client_authorization.http_basic_auth,
		headers=get_user_agent_header(oauth_params=oauth_parameters),
		data=refresh_token_params,
		timeout=default_request_timeout()
	)

	refresh_token_request.raise_for_status()

	refresh_token_request_json = refresh_token_request.json()

	access_token = refresh_token_request_json.get('access_token')
	if not access_token:
		print('Did not receive access token')
		sys.exit()
	potential_refresh_token = refresh_token_request_json.get('refresh_token')
	returned_scope = refresh_token_request_json.get('scope')
	if set(returned_scope.split(' ')) != set(oauth_parameters.scope.split(' ')):
		print(f'[{returned_scope}] does not match expected scope [{oauth_parameters.scope}]')
		sys.exit()
	returned_token_type = refresh_token_request_json.get('token_type')
	expected_token_type = 'bearer'
	if returned_token_type != expected_token_type:
		print(f'{returned_token_type} does not match expected token type {expected_token_type}')
		sys.exit()
	# Expected 2 weeks or less
	# https://www.oauth.com/oauth2-servers/access-tokens/access-token-lifetime/
	# Unix Epoch Seconds
	returned_expires_in = refresh_token_request_json.get('expires_in')
	if not isinstance(returned_expires_in, int):
		print(f'{returned_expires_in} is not of expected type')
		sys.exit()
	access_token_expiry = time_before_request + returned_expires_in

	return (
		access_token,
		access_token_expiry,
		potential_refresh_token
	)

def get_access_token(
	one_time_code: str,
	oauth_parameters: OAuthProcessParameters
) -> tuple[str, int, str]:
	"""Get access token from API"""
	# We have less time to use the access token than is returned:
	# 1. Response is received *after* the timeout is determined
	# 2. Processing the response takes time
	# 3. A call *using* the access token will also take time
	# As value should be positive, int conversion results in floor
	# Flooring is desired to remove unnecessary precision and adds safety margin to account for above
	time_before_request = int(time.time())
	access_token_uri = 'https://www.reddit.com/api/v1/access_token'
	access_token_params = {
		'grant_type': 'authorization_code',
		'code': one_time_code,
		'redirect_uri': oauth_parameters.redirect_uri
	}
	access_token_request = requests.post(
		url=access_token_uri,
		auth=oauth_parameters.client_authorization.http_basic_auth,
		headers=get_user_agent_header(oauth_params=oauth_parameters),
		data=access_token_params,
		timeout=default_request_timeout()
	)

	access_token_request.raise_for_status()

	access_token_request_json = access_token_request.json()

	access_token = access_token_request_json.get('access_token')
	if not access_token:
		print('Did not receive access token')
		sys.exit()
	potential_refresh_token = access_token_request_json.get('refresh_token')
	returned_scope = access_token_request_json.get('scope')
	if set(returned_scope.split(' ')) != set(oauth_parameters.scope.split(' ')):
		print( f'[{returned_scope}] does not match expected scope [{oauth_parameters.scope}]')
		sys.exit()
	returned_token_type = access_token_request_json.get('token_type')
	expected_token_type = 'bearer'
	if returned_token_type != expected_token_type:
		print(f'{returned_token_type} does not match expected token type {expected_token_type}')
		sys.exit()
	# Expected 2 weeks or less
	# https://www.oauth.com/oauth2-servers/access-tokens/access-token-lifetime/
	# Unix Epoch Seconds
	returned_expires_in = access_token_request_json.get('expires_in')
	if not isinstance(returned_expires_in, int):
		print(f'{returned_expires_in} is not of expected type')
		sys.exit()
	access_token_expiry = time_before_request + returned_expires_in

	return (
		access_token,
		access_token_expiry,
		potential_refresh_token
	)

def main() -> None:
	"""Main"""

	local_port = 7979

	(
		(
			client_id,
			client_secret,
			user_agent,
			socketserver_allow_reuse_address
		),
		(
			access_token,
			access_token_expiry_epoch,
			refresh_token
		)
	) = get_config()

	oauth_parameters = OAuthProcessParameters(
		client_authorization=ClientAuthorization(client_id=client_id, client_secret=client_secret),
		user_agent=user_agent,
		socketserver_allow_reuse_address=socketserver_allow_reuse_address,
		redirect_uri=f'http://localhost:{local_port}',
		scope='identity read report',
		duration=OAuthAuthorizationDuration.PERMANENT,
		token=TokenDetails(
			access_token=access_token,
			access_token_expiry_epoch=access_token_expiry_epoch,
			refresh_token=refresh_token
		)
	)

	if oauth_parameters.token.need_refresh_access_token():
		(
			oauth_parameters.token.access_token,
			oauth_parameters.token.access_token_expiry_epoch,
			oauth_parameters.token.refresh_token
		) = refresh_access_token(oauth_parameters=oauth_parameters)
		check_update_config(oauth_parameters=oauth_parameters)

	if oauth_parameters.token.has_valid_access_token():
		filter_front_page(oauth_parameters=oauth_parameters)
	else:
		unique_auth_req_id = str(uuid4())
		# Potential race condition - listener started after
		prompt_user_authorization(oauth_parameters, unique_auth_req_id=unique_auth_req_id)
		# Token retrieval
		start_server(
			local_port=local_port,
			oauth_parameters=oauth_parameters,
			unique_auth_req_id=unique_auth_req_id
		)

if __name__ == '__main__':
	main()
